# Game Of Life - Java

A simple Java program that simulates a cellular automaton, keeping it as straightforward as possible.

Our universe for the simulationm is a fixed-size grid (e.g. 10x10). If you need more space for more complex patterns, just increase it.

## **Rules of Game of Life**

1. Birth: A dead cell with exactly three live neighbors becomes a live cell.
2. Survival: A live cell with two or three live neighbors stays alive.
3. Death: In all other cases, a cell dies or remains dead.

The behavior, where the simulation oscillates between two states, is actually a known and expected pattern in Conway's Game of Life. This phenomenon occurs with certain initial configurations that lead to stable or repeating patterns rather than evolving into more complex or chaotic states. The behavior of the system is highly dependent on its initial setup. Here's a brief overview of the types of patterns you might encounter:

### Block (Stable Structures) 
These don't change after they appear. An example is the "block" pattern, which is a 2x2 square of live cells that remains unchanged through subsequent generations.
This initial configuration outputs a block (stable structure)
```java
grid[1][1] = true;
grid[1][2] = true;
grid[2][1] = true;
grid[2][2] = true;
```

### Blinker (Oscillators)
These are patterns that return to their initial state after a certain number of generations, creating a loop. The simplest one is the "blinker," which is a straight line of three cells that flips between vertical and horizontal states. It sounds like your configuration might be creating an oscillator.
This initial configuration outputs an oscillator (blinker):
```java
grid[1][2] = true;
grid[2][2] = true;
grid[3][2] = true;
```

### Glider (Spaceships)
These patterns move across the grid over time. The "glider" is a classic example that shifts one cell diagonally every four generations.
```java
grid[1][2] = true;
grid[2][3] = true;
grid[3][1] = true;
grid[3][2] = true;
grid[3][3] = true;
```

### R-Pentomino (Infinite Growth)
The R-pentomino is a small pattern that grows for 1103 generations before stabilizing, producing a complex pattern that includes spaceships and oscillators. Despite its simple start, the R-pentomino leads to surprisingly complex behavior.
```java
grid[1][2] = true;
grid[1][3] = true;
grid[2][1] = true;
grid[2][2] = true;
grid[3][2] = true;
```

If you're aiming to see more complex and varied behaviors, consider starting with a more randomized grid or specific patterns known to generate complexity, like the R-pentomino or Gosper's Glider Gun if you're feeling adventurous. These patterns can lead to a much more dynamic and unpredictable evolution of the grid.