package patterns;

public class Blinker {
    // This is our playing field or "world" where the game takes place.
    final static int SIZE = 20; // The size of our world. It's a square, so SIZE x SIZE.
    static boolean[][] grid = new boolean[SIZE][SIZE]; // The grid is our world map, showing which houses are lit.

    public static void main(String[] args) throws InterruptedException {

        // This initial configuration outputs an oscillator (blinker)
        grid[1][2] = true;
        grid[2][2] = true;
        grid[3][2] = true;

        // Simulate for 10 generations
        for (int gen = 0; gen < 10; gen++) {
            System.out.println("Generation: " + (gen + 1));
            simulate();
            printGrid();
            Thread.sleep(1000); // Wait for 1000 milliseconds (1 second) before the next generation
            System.out.println(); // Print a blank line between generations for readability
        }
    }

    static void simulate() {
        // We're about to see how our world changes.
        boolean[][] newGrid = new boolean[SIZE][SIZE]; // We prepare a new map to note down changes.
        for (int i = 0; i < SIZE; i++) { // Look at every row...
            for (int j = 0; j < SIZE; j++) { // And every column in our world.
                int liveNeighbors = countLiveNeighbors(i, j); // Count how many neighbors are alive around a house.
                if (grid[i][j]) { // If someone is home (cell is alive)...
                    // They stay home if they have 2 or 3 neighbors, else they leave (die).
                    newGrid[i][j] = liveNeighbors == 2 || liveNeighbors == 3;
                } else { // If the house is empty...
                    // A new person moves in (birth) if exactly 3 neighbors are around.
                    newGrid[i][j] = liveNeighbors == 3;
                }
            }
        }
        grid = newGrid; // Our new map becomes our current world.
    }

    static int countLiveNeighbors(int x, int y) {
        // We're going to see how friendly each house is by counting their neighbors.
        int count = 0; // Start with no friends.
        for (int i = -1; i <= 1; i++) { // Look left, right, and stay put.
            for (int j = -1; j <= 1; j++) { // Look up, down, and stay put.
                if (i == 0 && j == 0)
                    continue; // Skip your own house.
                int nx = x + i, ny = y + j; // The neighbor's address.
                // If the neighbor's house is on the map and someone is home...
                if (nx >= 0 && nx < SIZE && ny >= 0 && ny < SIZE && grid[nx][ny]) {
                    count++; // They've got one more friend!
                }
            }
        }
        return count; // Tell us how many friends they have.
    }

    static void printGrid() {
        // Let's draw a map of our world.
        for (int i = 0; i < SIZE; i++) { // For each row...
            for (int j = 0; j < SIZE; j++) { // And each column...
                System.out.print(grid[i][j] ? "O  " : ".  "); // Show a light if someone's home, or a dot if not.
            }
            System.out.println(); // Move to the next row of the map.
        }
    }
}
